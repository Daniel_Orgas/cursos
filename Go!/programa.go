package main

import ("fmt")

func main()  {

  fmt.Println("**********Mi Programa con GO**********");

//   edad,_ := strconv.Atoi(os.Args[2]);  /* pasamos el parametro de la edad que
//                                           esta en la posicion 2 además
//                                           convertimos la edad a integer con Atoi
//                                           y la guardamos en varieble y el error
//                                           lo cachamos y lo desechamos con "_"*/
//
//   fmt.Println("Hola "+os.Args[1]+" Bienvenido al programa en go");
//   /*cachamos el primer parametro pasado por la consola que esta en la posicion 1*/
//
//   if edad >= 18 && edad != 25 && edad !=99{
//     fmt.Println("Eres mayor de edad");
//   }else if edad == 25{
//     fmt.Println("Tienes 25 años");
//   }else if edad == 99 {
//     fmt.Println("Pronto moriras");
//   }else{
//     fmt.Println("eres menor de edad")
//   }
/*
  numero,_ := strconv.Atoi(os.Args[1]);

  if numero % 2 ==0{
    fmt.Println("Numero Par");
  }else{
    fmt.Println("Numero Impar");
  }

*/
peliculas := []string{"pelicula 1", "pelicula 2", "pelicula 3", "pelicula 4"};
/*
for i := 0; i<len(peliculas); i++{

  if i % 2 ==0{
    fmt.Println(i,"LA pelicula "+ peliculas[i] +" es Par" );
  }else{
    fmt.Println(i,"LA pelicula "+ peliculas[i] +" Impar");
  }

}*/

for _, pelicula := range peliculas{
  fmt.Println(pelicula);
}

 }
