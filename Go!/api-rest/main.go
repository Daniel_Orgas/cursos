package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	// Creacion de Rutas
	router := NewRouter()
	/*
	   http.HandleFunc("/" , func(w http.ResponseWriter, r *http.Request){
	     fmt.Fprintf(w, "Hola mundo desde mi servidor web con go")
	     })
	*/
	//pasamos la Ruta como parametro
	server := http.ListenAndServe(":8080", router)

	log.Fatal(server)
	fmt.Println("El servidor esta corriendo")
}
