package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func getSession() *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost")

	if err != nil {
		panic(err)
	}
	return session
}

// creamos una variablea para establecer la conexion
//creamos la base de datos en mongo y el nombre de la coleccion de datos
var collection = getSession().DB("curso_go").C("movies")

func Index(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "<h1>Hola mundo desde mi servidor web con go</h1>")
}

func MovieList(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "<h1>Listado de peliculas</h1>");

	var results []Movie

	err := collection.Find(nil).Sort("-_id").All(&results)

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println(results)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)
}

func MovieShow(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)    // recogemos las variables que tengamos em la url
	movie_id := params["id"] // re

	if !bson.IsObjectIdHex(movie_id) {
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(movie_id)
	results := Movie{}
	err := collection.FindId(oid).One(&results)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)

}

func MovieAdd(w http.ResponseWriter, r *http.Request) {
	//recogemos los datos del body que llgan por la Request y los decodificamos con json
	decoder := json.NewDecoder(r.Body)

	var movie_data Movie               //creamos una variable para los datos recojidos
	err := decoder.Decode(&movie_data) // decodificamos
	if err != nil {
		panic(err) // comprabamos si hay un error
	}

	defer r.Body.Close() // ceramos el body
	//guardar en base de datos

	// llamamos la conexion  la base de datos e insertamos los datos los datos caturados
	err = collection.Insert(movie_data)

	// Comprobamos si hay un error si lo hay regresa un cod 500 y se para el progrma
	if err != nil {
		w.WriteHeader(500)
		return
	}
	// Si todo sale Bien indicamos en content type y regresamos un codigo 200
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	// devuelve un objeo json
	json.NewEncoder(w).Encode(movie_data)

}

func MovieUpdate(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)    // recogemos las variables que tengamos em la url
	movie_id := params["id"] // re

	if !bson.IsObjectIdHex(movie_id) {
		w.WriteHeader(404)
		return
	}
	oid := bson.ObjectIdHex(movie_id)

	decoder := json.NewDecoder(r.Body)

	var movie_data Movie
	err := decoder.Decode(&movie_data)

	if err != nil {
		panic(err)
		w.WriteHeader(404)
		return
	}

	defer r.Body.Close()

	// AQUI
	document := bson.M{"_id": oid}
	change := bson.M{"$set": movie_data}
	err = collection.Update(document, change)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	results := Movie{}
	err = collection.FindId(oid).One(&results)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)

}

type Message struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func (this *Message) setStatus(data string) {
	this.Status = data
}
func (this *Message) setMessage(data string) {
	this.Message = data
}

func MovieRemove(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)    // recogemos las variables que tengamos em la url
	movie_id := params["id"] // re

	if !bson.IsObjectIdHex(movie_id) {
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(movie_id)

	err := collection.RemoveId(oid)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	// results := Message{"Success", "La pelicula"+movie_id+"borrada"}
	message := new(Message)
	message.setStatus("Success")
	results := message
	message.setMessage("La pelicula" + movie_id + "borrada")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)

}
