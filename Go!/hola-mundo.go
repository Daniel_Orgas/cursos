
package main

import "fmt"


func main(){

  // declaramos slice
  peliculas := []string {
    "La verdad duele",
    "ciudadano ejemplar",
    "batman",
    "superman",
  }


  peliculas = append(peliculas,"Sin limite");
  peliculas = append(peliculas,"camp rock");

  fmt.Println(peliculas);
  fmt.Println(develeperTexto());


}

// funciones con return
func develeperTexto() (dato string, dato3 string) {
  dato = "Daniel"
  dato3 = "Orgas"
  return // ya reconoce que tiene que devolver dato y dato3
//  return dato, dato3;
}
