const Model = require('./model');


function addUser(user){
    const myUser = new Model(user);
    return myUser.save();

}
async function getMessages(){
   
    // const messages = await Model.find();
    // return messages;
    return Model.find();
    
}



module.exports = {
    add: addUser,
    get: getMessages,
}