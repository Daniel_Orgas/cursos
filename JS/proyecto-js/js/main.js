$(document).ready(function() {

  if (window.location.href.indexOf('index') > -1) {

    $('.galeria').bxSlider({
      mode: 'fade',
      captions: true,
      slideWidth: 1200,
      pager: false

    });

  }


  //Posts
  if (window.location.href.indexOf('index') > -1) {
    var posts = [

      {
        title: 'Prueba de Titulo 1',
        date: 'Publicado el dia ' + moment().date() + " de " + moment().format('MMMM') + ' del ' + moment().format('YYYY'),
        contect: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },
      {
        title: 'Prueba de Titulo 2',
        date: 'Publicado el dia ' + moment().date() + " de " + moment().format('MMMM') + ' del ' + moment().format('YYYY'),
        contect: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },
      {
        title: 'Prueba de Titulo 3',
        date: 'Publicado el dia ' + moment().date() + " de " + moment().format('MMMM') + ' del ' + moment().format('YYYY'),
        contect: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },
      {
        title: 'Prueba de Titulo 4',
        date: 'Publicado el dia ' + moment().date() + " de " + moment().format('MMMM') + ' del ' + moment().format('YYYY'),
        contect: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },
      {
        title: 'Prueba de Titulo 5',
        date: 'Publicado el dia ' + moment().date() + " de " + moment().format('MMMM') + ' del ' + moment().format('YYYY'),
        contect: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },
      {
        title: 'Prueba de Titulo 6',
        date: 'Publicado el dia ' + moment().date() + " de " + moment().format('MMMM') + ' del ' + moment().format('YYYY'),
        contect: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },
      {
        title: 'Prueba de Titulo 7',
        date: 'Publicado el dia ' + moment().date() + " de " + moment().format('MMMM') + ' del ' + moment().format('YYYY'),
        contect: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      },

    ];


    posts.forEach((item, index) => {

      var post = `
                  <article class="post">

                          <h2>${item.title}</h2>
                          <span class="date">${item.date}</span>
                          <p>${item.contect}</p>

                          <a href="#" class="button-more">Leer más</a>
                  </article>

      `;
      $('#post').append(post);

    });

  }


  var theme = $("#theme");


  $("#to-green").click(function() {
    ;
    var tema = theme.attr("href", "css/green.css");
    localStorage.setItem("grea", tema);
  });

  $("#to-red").click(function() {
    theme.attr("href", "css/red.css");
  });

  $("#to-blue").click(function() {
    theme.attr("href", "css/blue.css");
  });

  $('.subir').click(function(e) {
    e.preventDefault();
    $('html,body').animate({
      scrollTop: 0
    }, 500);
    return false;
  });


  $("#login form").submit(function() {

    var form_name = $("#form_name").val();

    localStorage.setItem("form_name", form_name);
  });


  var form_name = localStorage.getItem("form_name");

  if (form_name != null && form_name != "undefined") {
    var about_parrafo = $("#about p");

    about_parrafo.html("<strong>Welcome, " + form_name + "</strong>");
    about_parrafo.append("<a href='#' id='logout'> Cerrar Sesión</a>");

    $("#login").hide;

    $("#logout").click(function() {
      localStorage.clear();
      location.reload();
    });
  }
      // ACORDEON
  if (window.location.href.indexOf('about') > -1) {

    $("#acordeon").accordion();



  }

  if (window.location.href.indexOf('reloj') > -1) {

    setInterval(function(){
      var reloj = moment().format("hh:mm:ss");
      $("#reloj").html(reloj);
    },1000);
  }

  if (window.location.href.indexOf('contact') > -1) {

    $("form input[name='date']").datepicker({
      dateFormat: 'dd-mm-yy'
    });

    $.validate({
      modules : 'security',
      lang: 'es',
      errorMessagePosition: 'top',

    });
  }




});
