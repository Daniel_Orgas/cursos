//Interface 
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//decorador 
// function estampar(logo: string){
//     return function(target: Function){
//         target.prototype.estampado = function():void{
//             console.log("Camiseta estamapada con el logo de: "+logo);
//         }
//     }
// }
// // CLASE UN MOLDE DE UN OBJETO 
// @estampar('Gucci')
var Camiseta = /** @class */ (function () {
    // Contructor -> inicializamos los valores del objeto
    function Camiseta(color, modelo, marca, talla, precio) {
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }
    // MÉTODOS (FUNCIONES O ACCIONES DEL OBJETO)
    Camiseta.prototype.setColor = function (color) {
        this.color = color;
    };
    Camiseta.prototype.getColor = function () {
        return this.color;
    };
    return Camiseta;
}());
//Clase Hija
var Sudadera = /** @class */ (function (_super) {
    __extends(Sudadera, _super);
    function Sudadera() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Sudadera.prototype.setCapucha = function (capucha) {
        this.capucha = capucha;
    };
    Sudadera.prototype.getCapucha = function () {
        return this.capucha;
    };
    return Sudadera;
}(Camiseta));
var camiseta = new Camiseta("Rojo", "mangas", "nike", "ch", 50);
var sudadera = new Sudadera("Azul", "mangas", "nike", "ch", 50);
sudadera.setCapucha(true);
sudadera.setColor("verde");
console.log(camiseta, sudadera);
