// combinar tipos de datos
type letrasonumeros = string | number;


// string 

let cadena: string | number = "Daniel Orgas";
// number
cadena = 12;
let numero: number = 12;

let cualquier: any = "hola";


// Array
var lengujes: Array<string> = ["php", "js"];

let years: number[] = [12,14];

console.log(cadena,numero, lengujes, years);