
//Interface 

interface CamisetaBase{
    setColor(color);
    getColor();
}

//decorador 
// function estampar(logo: string){
//     return function(target: Function){
//         target.prototype.estampado = function():void{
//             console.log("Camiseta estamapada con el logo de: "+logo);
            
//         }
//     }
// }

// // CLASE UN MOLDE DE UN OBJETO 
// @estampar('Gucci')
class Camiseta implements CamisetaBase{
    //PROPIEDADES (CARACTERISTICAS )
    private color: string;
    private modelo: string;
    private marca: string;
    private talla: string;
    private precio: number;

    // Contructor -> inicializamos los valores del objeto
    constructor(color, modelo, marca, talla, precio){
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }

    // MÉTODOS (FUNCIONES O ACCIONES DEL OBJETO)
    public setColor(color){
        this.color = color;
    }
    public getColor(){
        return this.color;
    }
}

//Clase Hija
class Sudadera extends Camiseta{
    public capucha: boolean;
    
    setCapucha(capucha: boolean){
        this.capucha = capucha;
    }
    getCapucha():boolean{
        return this.capucha;
    }
}


var camiseta = new Camiseta("Rojo","mangas", "nike", "ch", 50);


var sudadera = new Sudadera("Azul","mangas", "nike", "ch", 50);
sudadera.setCapucha(true);
sudadera.setColor("verde");


console.log(camiseta, sudadera);
