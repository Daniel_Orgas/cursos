'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ProjectSchema = Schema({

    name: String,
    description: String,
    category: String,
    year: Number,
    langs: String,
    image: String
});

// Para poder utilizarlo fuera de este fichero 
// Mongoose pone en minusculas el primer parametro y lo hace plural Project-->projects
module.exports = mongoose.model('Project',ProjectSchema);


// MVC  SEPARAR LA LOGICA DE NOGOCIO DE LA INTERFAZ DEL USUARIO
//modelo: se conecta a la base de datos para guardar y consulta
//modelo. hay modelos de consulta 
// vista: encargada de mostrarle la info al usuario nuestra vista en este caso seran los json
//controlador: intermediario entre vista y modelo, comrolar las interaciones del usuario 