$(document).ready(function (){

  //Mover elemento por la pagina
  $(".elemento").draggable();

  //Redimencionar
  $(".elemento").resizable();

  //Seleccionar y ordenar elementos
  //$(".lista-seleccionable").selectable();
  $(".lista-seleccionable").sortable({update: (event, ui) =>{
      console.log("ha cambido la lista");
    }
  });

  //Drop
  $("#elemento-movido").draggable();

  $("#area").droppable({
    drop: function(){
      console.log("Has soltado algo desntro del area");
    }
  });

  //efectos
  $("#mostrar").click(function(){

    $(".caja-efectos").toggle("shake");

  });


  //Tooltips

  $(document).tooltip();

  //dialog
  $("#bpopup").click(function() {

    $("#popup").dialog();

  });

  //datepicker

  $("#calendario").datepicker();


  //tabs
  $("#pestanas").tabs();



});
