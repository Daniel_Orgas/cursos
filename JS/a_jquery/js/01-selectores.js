$(document).ready(function(){

  //Selector de Id
  $("#rojo").css("background","red")
            .css("color", "white");

  $("#amarillo").css("background","yellow")
            .css("color", "green");

  $("#verde").css("background","green")
            .css("color", "white");


//Selectores de Clase

var mi_clase = $(".zebra").css("padding", "5px");


$(".sin_borde").click(function(){
  console.log("Click_dado!!");
  $(this).addClass("zebra");
});

//Selectores de Etiquetas

var parrafos = $('p').css("cursor","pointer"); // Seleccionamos los parrafos

parrafos.click(function() {
  var th = $(this);
  if(!th.hasClass('grande'))
  {
      th.addClass("grande");
  }else{
      th.removeClass("grande");

  }

});
//Selectores de atributos

$('[title="Google"]').css('background','#ccc');
$('[title="Facebook"]').css('background','blue');

$(".este").click(function(){
  $(this).addClass("mia").css('background','green');
})
  //OTROS

  //$('p','a').addClass('margen-superior');
  var buscar = $('#elemento2').parent().parent().find('.resaltado');

  console.log(buscar);

});//Fin del ready
